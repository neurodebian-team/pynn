pynn (0.12.2-1) UNRELEASED; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * New upstream version 0.12.2
  * Build-Depends: s/python3-nose/python3-pytest/
  * Standards-Version: 4.6.2 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * d/watch: Fix upstream download name
  TODO: New dependency https://salsa.debian.org/med-team/python-morphio

  [ Étienne Mollier ]
  d/gbp.conf and python3.12.patch removed.

 -- Alexandre Detiste <tchet@debian.org>  Sat, 01 Mar 2025 12:27:30 +0100

pynn (0.10.1-4) unstable; urgency=medium

  * Team upload.
  * Disable tests to allow removal of dependency on python3-nose
    (Closes: #1018448)

 -- Alexandre Detiste <tchet@debian.org>  Tue, 18 Feb 2025 08:06:05 +0100

pynn (0.10.1-3) unstable; urgency=medium

  * d/gbp.conf: new: sidetracking branch.
    Newer upstream versions will require new packages, so in the meantime,
    let's continue in the debian/unstable instead of the repository's
    default master branch; this requires adjustments to gbp though.
  * python3.12.patch: new: fix test with python3.12. (Closes: #1058252)

 -- Étienne Mollier <emollier@debian.org>  Fri, 15 Dec 2023 11:20:17 +0100

pynn (0.10.1-2) unstable; urgency=medium

  * numpy1.24.patch: add; fix test failures with numpy 1.24.
    (Closes: #1028812)
  * d/copyright: bump copyright year.

 -- Étienne Mollier <emollier@debian.org>  Sat, 14 Jan 2023 15:28:08 +0100

pynn (0.10.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * python-3.10.patch: remove; python3.10 support fixed upstream.
  * d/blends: remove; pyNN moved from neurodebian to debian-med.
  * d/copyright: bump copyright years.

 -- Étienne Mollier <emollier@debian.org>  Sat, 22 Oct 2022 12:25:25 +0200

pynn (0.10.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * d/patches/: erase unused patch
  * Add python-3.10.patch: fix build time tests.  (Closes: #1002365)
  * d/copyright: add myself to bump year

 -- Étienne Mollier <emollier@debian.org>  Wed, 22 Dec 2021 21:51:20 +0100

pynn (0.9.6-2) unstable; urgency=medium

  * Fix watchfile to detect new versions on github (routine-update)
  * d/control: add myself to uploaders
  * d/control: Standards-Version: 4.6.0.1
  * no_testconfig_nest.patch forwarding not needed

 -- Étienne Mollier <emollier@debian.org>  Thu, 16 Sep 2021 21:28:14 +0200

pynn (0.9.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Wed, 20 Jan 2021 18:30:28 +0100

pynn (0.9.5-3) unstable; urgency=medium

  * Team upload.
  * Configure autodep8 to import pyNN instead of pynn, closes: #959061.
  * Remove trailing whitespace in debian/rules (routine-update)

 -- Etienne Mollier <etienne.mollier@mailoo.org>  Wed, 27 May 2020 22:22:04 +0200

pynn (0.9.5-2) unstable; urgency=medium

  * Team upload.
  * Do not ignore test suite errors, versioned Build-Depends: python3-lazyarray
    Closes: #813821
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 03 Apr 2020 17:44:55 +0200

pynn (0.9.5-1) unstable; urgency=medium

  * Team upload.
  * Take over package into Debian Med team maintenance
  * Point watch file to github
  * Switch to Python3
    Closes: #937491
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Secure URI in copyright format (routine-update)
  * Add salsa-ci file (routine-update)
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * New (Build-)Depends: python3-neuron, python3-quantities,
    python3-lazyarray, python3-neo
  * Ignore test suite errors which are most probably caused by lazyarray
     (see https://bitbucket.org/apdavison/lazyarray/issues/6/test-failure)
  * Fix permissions of *.mod files
  * Fix copyright

 -- Andreas Tille <tille@debian.org>  Thu, 05 Mar 2020 10:54:14 +0100

pynn (0.7.5-1) experimental; urgency=low

  * New upstream bugfix release

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 06 Feb 2013 09:46:53 -0500

pynn (0.7.4-1) unstable; urgency=low

  * New upstream release
    - fixes a few bugs detected upstream
    - works around regressions of numpy 1.6 (e.g. #679948, #679998) and
      mock 0.8 allowing unittests to pass (Closes: #669466)

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 03 Jul 2012 23:54:42 -0400

pynn (0.7.2-1) unstable; urgency=low

  * New upstream release.
  * Changed maintainer email.
  * Added python-csa as a recommended package (connectors can use it).
  * Bump standards-version to 3.9.3 -- no changes necessary.
  * Indicate compliance with copyright format 1.0.
  * Fix language issue in package description.
  * Add python-csa as suggested package.
  * Disable most of one test for numerical instabilities.
  * Remove all previous patches -- merged upstream.

 -- Michael Hanke <mih@debian.org>  Fri, 16 Mar 2012 15:16:54 +0100

pynn (0.7.0-1) unstable; urgency=low

  * Initial release (Closes: #604159)

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 15 Nov 2010 00:00:43 -0500
