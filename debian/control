Source: pynn
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Yaroslav Halchenko <debian@onerussian.com>,
           Michael Hanke <mih@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-numpy,
               python3-pytest <!nocheck>,
               python3-cheetah <!nocheck>,
               python3-jinja2 <!nocheck>,
               python3-neuron <!nocheck>,
               python3-quantities <!nocheck>,
               python3-lazyarray <!nocheck>,
               python3-neo <!nocheck>,
               python3-setuptools
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/pynn
Vcs-Git: https://salsa.debian.org/med-team/pynn.git
Homepage: https://neuralensemble.org/trac/PyNN
Rules-Requires-Root: no

Package: python3-pynn
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         python3-neuron,
         python3-quantities,
         python3-neo
Recommends: python3-jinja2,
            python3-cheetah,
            python3-lazyarray
Suggests: python3-brian,
          python3-csa
Description: simulator-independent specification of neuronal network models
 PyNN allows for coding a model once and run it without modification
 on any simulator that PyNN supports (currently NEURON, NEST, PCSIM
 and Brian).  PyNN translates standard cell-model names and parameter
 names into simulator-specific names.
